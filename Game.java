public class Game
{
 private String title;
 private String publisher;
 private double price;
 private double discountPercent;
 private String genre;
 
 //getset methods
 
 public Game(String title, String publisher, double price, double discountPercent, String genre)
 {
	 this.title = title;
	 this.publisher = publisher;
	 this.price = price;
	 this.discountPercent = discountPercent;
	 this.genre = genre;
 }
 public String getTitle()
 {
	 return this.title;
 }
 
  public String getPublisher()
 {
	 return this.publisher;
 }
  public void setPublisher(String value)
 {
	 this.publisher = value;
 }
 
  public double getPrice()
 {
	 return this.price;
 }
  public void setPrice(double value)
 {
	 this.price = value;
 }
 
   public double getDiscountPercent()
 {
	 return this.discountPercent;
 }
  public void setDiscountPercent(double value)
 {
	 this.discountPercent = value;
 }
 
  public String getGenre()
 {
	 return this.genre;
 }
  public void setGenre(String value)
 {
	 this.genre = value;
 }
 
 public void PrintDiscountedPrice()
 {
  double discountedPrice = price * (discountPercent / 100);
  System.out.println("The discounted price for " + title + " is: " + (price - discountedPrice));
 }
}