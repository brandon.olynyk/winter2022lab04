import java.util.Scanner;
public class Shop
{

 public static void main(String[] args)
 {
	   Scanner scan = new Scanner(System.in);
	   
	  Game[] games = new Game[4];
	  
	  //populate array
	  for(int i = 0; i < games.length; i++)
	  {
		   System.out.println("Please enter the title for game #" + (i + 1));
		   String title = scan.next();
		   System.out.println("Please enter the publisher for game #" + (i + 1));
		   String publisher = scan.next();
		   System.out.println("Please enter the genre for game #" + (i + 1));
		   String genre = scan.next();
		   System.out.println("Please enter the price for game #" + (i + 1));
		   double price = scan.nextDouble();
		   System.out.println("Please enter the discount in % for game #" + (i + 1));
		   double discountPercent = scan.nextDouble();
		   
		   games[i] = new Game(title, publisher, price, discountPercent, genre);
	  }
	  
	  System.out.println("For the last product,before the setters are called,");
	  printGameValues(games[3]);
	  games[3].PrintDiscountedPrice();
	  
	  //call setters
	  
		   System.out.println("Please enter the title for game number 4 again" );
		   String title = scan.next();
		   System.out.println("Please enter the publisher for game number 4 again");
		   String publisher = scan.next();
		   System.out.println("Please enter the genre for game number 4 again" );
		   String genre = scan.next();
		   System.out.println("Please enter the price for game number 4 again" );
		   double price = scan.nextDouble();
		   System.out.println("Please enter the discount in % for game number 4 again" );
		   double discountPercent = scan.nextDouble();
		   
		   games[3] = new Game(title, publisher, price, discountPercent, genre);
		   
		  System.out.println("For the last product,after the setters are called,");
		  printGameValues(games[3]);
 }
 
 public static void printGameValues(Game gameObect)
 {
	  System.out.println("The title is: " + gameObect.getTitle());
	  System.out.println("The publisher is " + gameObect.getPublisher());
	  System.out.println("The price is " + gameObect.getPrice());
	  System.out.println("The discount in percentage is " + gameObect.getDiscountPercent());
	  System.out.println("The genre is " + gameObect.getGenre());
 }
 
}